using UnityEngine;

public class BackgroundMusic : MonoBehaviour
{

    private static BackgroundMusic _backgroundMusic;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (_backgroundMusic == null)
        {
            _backgroundMusic = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
