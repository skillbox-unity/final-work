using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingAudioMixer : MonoBehaviour
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Slider _backgroundMusicSlider;
    [SerializeField] private Slider _soundEffectSlider;

    private void Start()
    {
        _backgroundMusicSlider.value = PlayerPrefs.GetFloat("BackgroundMusicVolume");
        _soundEffectSlider.value = PlayerPrefs.GetFloat("SoundEffectVolume");
    }

    public void SetMusicVolume(float volume)
    {
        _audioMixer.SetFloat("BackgroundMusicVolume", volume);
        PlayerPrefs.SetFloat("BackgroundMusicVolume", volume);
    }

    public void SetSoundEffectVolume(float volume)
    {
        _audioMixer.SetFloat("SoundEffectVolume", volume);
        PlayerPrefs.SetFloat("SoundEffectVolume", volume);

    }
}