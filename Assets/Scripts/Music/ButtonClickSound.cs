using UnityEngine;

public class ButtonClickSound : MonoBehaviour
{
    [SerializeField] private AudioSource _audio;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void PlaySoundOnClick()
    {
        _audio.Play();
    }
}