using UnityEngine;

public class Sounds : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _reloading;
    [SerializeField] private AudioClip _fire;
    [SerializeField] private AudioClip _defeate;

    public void PlayReloading()
    {

        _audioSource.PlayOneShot(_reloading);
    }

    public void PlayFire()
    {
        _audioSource.PlayOneShot(_fire);
    }

    public void PlayDefeate()
    {
        _audioSource.PlayOneShot(_defeate);
    }
}