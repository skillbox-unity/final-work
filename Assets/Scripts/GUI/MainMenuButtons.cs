﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    [SerializeField] private GameObject _settingsPanel;
    [SerializeField] private GameObject _mainMenuPanel;
    [SerializeField] private GameObject _questionPanel;

    void Start()
    {
        StartStatePanel();
    }

    private void StartStatePanel()
    {
        _settingsPanel.SetActive(false);
        _mainMenuPanel.SetActive(true);
        _questionPanel.SetActive(false);
    }

    public void OnQuitGameButtonClick()
    {
        Application.Quit();
    }

    public void OnStartGameButtonClick()
    {
        SceneManager.LoadScene("LevelScene");
    }

    public void OnSettingsButtonClick()
    {
        _settingsPanel.SetActive(true);
        _mainMenuPanel.SetActive(false);
        _questionPanel.SetActive(false);
    }

    public void OnQuestionButtonClick()
    {
        _settingsPanel.SetActive(false);
        _mainMenuPanel.SetActive(false);
        _questionPanel.SetActive(true);
    }

    public void OnNoButtonClick()
    {
        StartStatePanel();
    }
}
