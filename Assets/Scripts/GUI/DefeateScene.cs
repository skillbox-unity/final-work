﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Hero
{
    internal class DefeateScene : MonoBehaviour
    {
        public void Show()
        {
            SceneManager.LoadScene("LoseScene");
        }
    }
}
