using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject _pausePanel;

    public bool Paused { get; private set; }

    void Start()
    {
        Paused = false;
        _pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void PauseGame()
    {
        if (Paused)
        {
            Paused = !Paused;
            Time.timeScale = 1;
            _pausePanel.SetActive(false);

        }
        else 
        {
            Paused = !Paused;
            Time.timeScale = 0;
            _pausePanel.SetActive(true);
        }

    }
}
