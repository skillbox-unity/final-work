﻿using Assets.Scripts.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.GUI
{
    internal class AmmoText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _ammoText;

        public void SetAmmo(int ammo)
        {
            _ammoText.text = $"{ammo}";
        }
    }
}
