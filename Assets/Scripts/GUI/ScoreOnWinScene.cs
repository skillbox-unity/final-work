using TMPro;
using UnityEngine;

public class ScoreOnWinScene : MonoBehaviour
{
    [SerializeField] private TMP_Text _scoreText;

    void Start()
    {
        int score = PlayerPrefs.GetInt("Score");
        Debug.Log(score);
        _scoreText.text = $"Score: {score}";
    }
}
