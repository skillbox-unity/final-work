﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.GUI
{
    internal class ScoreText : MonoBehaviour
    {
        [SerializeField] private TMP_Text _scoreText;

        public void SetScore(int score)
        {
            _scoreText.text = $"Score: {score}";
        }
    }
}
