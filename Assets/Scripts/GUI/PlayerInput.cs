﻿using UnityEngine;

namespace Assets.Scripts.GUI
{
    internal class PlayerInput : MonoBehaviour
    {
        [SerializeField] private Pause _pause;

        void Update()
        {
            CheckEscapeButton();
        }

        private void CheckEscapeButton()
        {
            if (Input.GetKeyDown(KeyCode.Escape)) _pause.PauseGame();
        }
    }
}
