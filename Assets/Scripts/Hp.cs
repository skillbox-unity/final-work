﻿using Assets.Scripts.Enemies;
using Assets.Scripts.Hero;
using UnityEngine;

namespace Assets.Scripts
{
    internal class Hp : MonoBehaviour
    {
        [SerializeField] private float _maxHp;
        [SerializeField] private HpBar _hpBarController;
        [SerializeField] private Enemy _enemy;
        [SerializeField] private HeroActions _hero;

        private float _currentHp;

        private void Awake()
        {
            _enemy = GetComponent<Enemy>();
        }

        void Start()
        {
            _currentHp = _maxHp;
            _hpBarController.SetMaxHP(_maxHp);
        }

        public void AddHp(float addedHp)
        {
            _currentHp = SetHpAfterHeal(addedHp);
            _hpBarController.SetCurrentHP(_currentHp);
        }

        public void TakeDamage(float damage, bool isHero)
        {
            _currentHp -= damage;
            _hpBarController.SetCurrentHP(_currentHp);
            TakeCharacterDamage(isHero);
        }

        private void TakeCharacterDamage(bool isHero)
        {
            if (isHero)
            {
                if (!CheckIsAlive())
                {
                    _hero.Defeate();
                }
                else
                {
                    _hero.TakeDamage();
                }
            }
            else
            {
                if (!CheckIsAlive())
                {
                    _enemy.Defeate();
                }
                else
                {
                    _enemy.TakeDamage();
                }
            }
        }

        private float SetHpAfterHeal(float addedHp)
        {
            float tempHp = _currentHp + addedHp;
            if (tempHp > _maxHp) return _maxHp;
            else return tempHp;
        }

        private bool CheckIsAlive()
        {
            if (_currentHp <= 0)
                 return false;
            else
                return true;
        }
    }
}
