﻿using Assets.Scripts.Hero;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
    internal class EnemyAttackTrigger : MonoBehaviour
    {
        [SerializeField] private Enemy _controller;

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out HeroActions heroActions))
            {
                float difX = gameObject.transform.position.x - collision.transform.position.x;
                _controller.Attack(difX);
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.TryGetComponent(out HeroActions heroActions))
            {
                _controller.StopAttack();
            }
        }
    }
}
