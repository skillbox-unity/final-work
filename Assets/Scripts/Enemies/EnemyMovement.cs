﻿using UnityEngine;

namespace Assets.Scripts.Enemies
{
    internal class EnemyMovement : MonoBehaviour
    {
        [SerializeField] private float _speed;
        [SerializeField] private float _radius;

        public void Move(float difX)
        {
            Vector2 target = new Vector2(transform.position.x + difX - _radius, transform.position.y);
            float step = _speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, target, step);
        }
    }
}
