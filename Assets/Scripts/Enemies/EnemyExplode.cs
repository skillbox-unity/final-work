﻿using Assets.Scripts.Hero;
using UnityEngine;
using UnityEngine.UIElements;

namespace Assets.Scripts.Enemies
{
    internal class EnemyExplode : MonoBehaviour
    {
        [SerializeField] private float _damage;
        [SerializeField] private float _explosionForce;
        [SerializeField] private Enemy _enemy;
        [SerializeField] private LayerMask _layerMask;

        private float _upForce = 0.5f;

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Character")) _enemy.Defeate();
        }

        public void Explode()
        {
            CircleCollider2D circleCollider2D = gameObject.GetComponent<CircleCollider2D>();
            Vector2 position = new Vector2(transform.position.x + circleCollider2D.offset.x, transform.position.y + circleCollider2D.offset.y);
            Collider2D[] colliders = Physics2D.OverlapCircleAll(position, circleCollider2D.radius, _layerMask);
            foreach (Collider2D hit in colliders)
            {
                if (hit.attachedRigidbody != null)
                {
                    if (hit.TryGetComponent(out HeroActions hero))
                    {
                        hit.GetComponent<Hp>().TakeDamage(_damage, true);
                    }
                    else
                    {
                        if (hit.TryGetComponent(out Enemy enemy))
                        {
                            hit.GetComponent<Hp>().TakeDamage(_damage, false);
                        }
                    }

                    Vector2 direction = new Vector2(hit.transform.position.x - transform.position.x, hit.transform.position.y - transform.position.y + _upForce);
                    hit.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(direction * _explosionForce, ForceMode2D.Impulse);
                }
            }
        }
    }
}
