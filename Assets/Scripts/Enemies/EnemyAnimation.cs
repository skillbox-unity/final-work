﻿using UnityEngine;

namespace Assets.Scripts.Enemies
{
    [RequireComponent(typeof(Animator))]
    internal class EnemyAnimation : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        public bool IsFire { private get; set; }
        public bool IsMoving { private get; set; }

        void Update()
        {
            _animator.SetBool("isFire", IsFire);
            _animator.SetBool("isMoving", IsMoving);
        }

        public void TakeDamage()
        {
            _animator.SetTrigger("makedHurt");
        }

        public void Defeate()
        {
            _animator.SetBool("isDefeated", true);
        }
    }
}
