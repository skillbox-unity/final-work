﻿using Assets.Scripts.Characters;
using Assets.Scripts.Hero;
using UnityEngine;

namespace Assets.Scripts.Enemies
{
    [RequireComponent(typeof(EnemyAnimation))]
    internal class Enemy : MonoBehaviour
    {
        [SerializeField] private EnemyAnimation _animations;
        [SerializeField] private CharacterAttack _attack;
        [SerializeField] private EnemyMovement _movement;
        [SerializeField] private EnemyExplode _explode;
        [SerializeField] private Sounds _sounds;
        [SerializeField] private bool _isMelee = false;
        [SerializeField] private int _scores = 1000;
        [SerializeField] private Scores _scoreController;

        private bool _isAlive;
        private SpriteRenderer _spriteRenderer;

        private void Start()
        {
            _isAlive = true;
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _attack.OnBulletsCountChange += BulletsCountChanged;
        }

        public void Attack(float difX)
        {
            if (_isAlive)
            {
                SetFlipXFromDirection(difX);
                _attack.Attack(GetDirectionFromFlipX());
                _animations.IsFire = true;
                if (_isMelee)
                {
                    _movement.Move(-difX);
                    _animations.IsMoving = true;
                }
            }
        }

        public void StopAttack()
        {
            _animations.IsFire = false;
            if (_isMelee) _animations.IsMoving = false;
        }

        public void TakeDamage()
        {
            _animations.TakeDamage();
        }

        public void Defeate()
        {
            if (_isAlive)
            {
                _scoreController.AddPoints(_scores);
                DestroyOnDefeate();
                _animations.Defeate();
            }
        }

        private void SetFlipXFromDirection(float difX)
        {
            if (difX < 0) _spriteRenderer.flipX = false;
            else _spriteRenderer.flipX = true;
        }

        private float GetDirectionFromFlipX()
        {
            return _spriteRenderer.flipX ? -1 : 1;
        }

        private void DestroyOnDefeate()
        {
            _isAlive = false;
            gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            gameObject.tag = "Dead";
        }

        private void DestroyOnExplode()
        {
            foreach (Transform child in gameObject.transform)
            {
                Destroy(child.gameObject);
            }
        }

        public void Explode()
        {
            _explode.Explode();
            _sounds.PlayDefeate();
            DestroyOnExplode();
        }

        private void BulletsCountChanged(int currentAmmo, bool isSetMaxBullet)
        {
            if (!isSetMaxBullet) _sounds.PlayFire();
        }
    }
}
