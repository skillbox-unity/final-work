﻿using UnityEngine;

namespace Assets.Scripts.Objects
{
    internal class Elevator : MonoBehaviour
    {
        [SerializeField] private SliderJoint2D _slider;
        [SerializeField] private Transform _min;
        [SerializeField] private Transform _max;

        private void Update()
        {

            if (transform.localPosition.y > _max.localPosition.y) ReverseMotor(1);
            else if (transform.localPosition.y < _min.localPosition.y) ReverseMotor(-1);

        }

        private void ReverseMotor(int direction)
        {
            JointMotor2D motor = _slider.motor;
            motor.motorSpeed = direction;
            _slider.motor = motor;
        }
    }
}
