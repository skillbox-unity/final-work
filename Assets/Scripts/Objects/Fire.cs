﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Enemies;
using Assets.Scripts.Hero;
using UnityEngine;

namespace Assets.Scripts.Objects
{
    internal class Fire : MonoBehaviour
    {
        [SerializeField] private int _damage = 20;

        private float _maxTime = 1.0f;
        private float _currentTime = 0.0f;
        private bool _canFire = true;

        void Start()
        {
            _currentTime = _maxTime;
        }

        void Update()
        {
            if(_currentTime > 0 && !_canFire)
            {
                _currentTime -= Time.deltaTime;
                _canFire = false;
            }
            else
            {
                _canFire = true;
                _currentTime = _maxTime;
            }
        }

        private void OnTriggerStay2D(Collider2D collision)
        {
            if (_canFire)
            {
                if (collision.TryGetComponent(out HeroActions hero))
                {
                    collision.gameObject.GetComponent<Hp>().TakeDamage(_damage, true);
                    _canFire = false;
                }
                else
                {
                    if (collision.TryGetComponent(out Enemy enemy))
                    {
                        collision.gameObject.GetComponent<Hp>().TakeDamage(_damage, false);
                        _canFire = false;
                    }
                }
            }
        }
    }
}
