using Assets.Scripts;
using Assets.Scripts.Enemies;
using Assets.Scripts.Hero;
using UnityEngine;

public class FallBoxDamage : MonoBehaviour
{
    [SerializeField] private int _damage = 1000;

    public void FallBox()
    {
        GetComponentInParent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out HeroActions hero))
        {
            collision.gameObject.GetComponent<Hp>().TakeDamage(_damage, true);
            Destroy(transform.parent.GetComponent<BoxCollider2D>());
        }
        else
        {
            if (collision.TryGetComponent(out Enemy enemy))
            {
                collision.gameObject.GetComponent<Hp>().TakeDamage(_damage, false);
                Destroy(transform.parent.GetComponent<BoxCollider2D>());
            }
        }
    }
}
