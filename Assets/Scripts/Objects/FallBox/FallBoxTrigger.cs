using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallBoxTrigger : MonoBehaviour
{
    [SerializeField] private FallBoxDamage _fallBoxDamage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Character"))
        {
            _fallBoxDamage.FallBox();
        }
    }
}
