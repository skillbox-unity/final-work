using Assets.Scripts;
using UnityEngine;

public class HpPack : MonoBehaviour
{
    [SerializeField] float addedHp;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Hp hp))
        {
            hp.AddHp(addedHp);
            Destroy(gameObject);
        }
    }
}
