using Assets.Scripts.Hero;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out HeroActions hero))
        {
            SceneManager.LoadScene("WinScene");
        }
    }
}
