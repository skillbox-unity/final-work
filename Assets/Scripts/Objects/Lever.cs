using Assets.Scripts.Objects.Bullet;
using UnityEngine;

public class Lever : MonoBehaviour
{
    [SerializeField] private GameObject _door;
    [SerializeField] private Sprite _activatedSprite;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Bullet bullet))
        {
            Destroy(_door);
            Destroy(bullet.gameObject);
            gameObject.GetComponent<SpriteRenderer>().sprite = _activatedSprite;
        }
    }
}
