﻿using UnityEngine;

namespace Assets.Scripts.Objects.Bullet
{
    internal class Bullet : MonoBehaviour
    {
        [SerializeField] private float _speedBullet;
        [SerializeField] private GameObject _bullet;

        public void Create(Transform bulletPointTransform, float directionX)
        {
            Vector3 currentPosition = BulletPositionOnDirection(bulletPointTransform, directionX);
            GameObject currentBullet = Instantiate(_bullet, currentPosition, Quaternion.identity);

            Rigidbody2D currentBulletRigidbody2D = currentBullet.GetComponent<Rigidbody2D>();
            currentBulletRigidbody2D.velocity = new Vector2(_speedBullet * directionX, currentBulletRigidbody2D.velocity.y);

            SpriteRenderer currentSpriteRenderer = currentBullet.GetComponent<SpriteRenderer>();
            currentSpriteRenderer.flipX = directionX > 0 ? false : true;
        }

        private Vector3 BulletPositionOnDirection(Transform bulletPointTransform, float directionX)
        {
            if (directionX > 0)
            {
                return new Vector3(bulletPointTransform.position.x, bulletPointTransform.position.y, 0);
            }
            else
            {
                return new Vector3(bulletPointTransform.position.x - bulletPointTransform.localPosition.x, bulletPointTransform.position.y, 0);
            }
        }
    }
}
