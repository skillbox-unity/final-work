using Assets.Scripts;
using Assets.Scripts.Enemies;
using Assets.Scripts.Hero;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    [SerializeField] private float _damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out HeroActions hero))
        {
            collision.GetComponent<Hp>().TakeDamage(_damage, true);
            Destroy(gameObject);
        }
        else
        {
            if (collision.TryGetComponent(out Enemy enemy))
            {
                collision.GetComponent<Hp>().TakeDamage(_damage, false);
                Destroy(gameObject);
            }
        }
    }
}
