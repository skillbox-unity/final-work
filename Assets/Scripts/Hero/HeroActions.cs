﻿using Assets.Scripts.Characters;
using UnityEngine;

namespace Assets.Scripts.Hero
{
    [RequireComponent(typeof(HeroMovement))]
    [RequireComponent(typeof(CharacterAttack))]
    [RequireComponent(typeof(HeroAnimations))]
    [RequireComponent(typeof(GroundCheck))]
    public class HeroActions : MonoBehaviour
    {
        [SerializeField] private HeroMovement _movement;
        [SerializeField] private CharacterAttack _attack;
        [SerializeField] private HeroAnimations _animations;
        [SerializeField] private Sounds _sounds;
        [SerializeField] private GroundCheck _groundCheck;
        [SerializeField] private Ammo _ammo;
        private Vector3 _position;
        private float _directionX;
        private bool _isReloading;
        private bool _isDefeated;

        void Awake()
        {
            _isReloading = false;
            _isDefeated = false;
            
        }

        private void Start()
        {
            _attack.OnBulletsCountChange += BulletsCountChanged;
        }

        void Update() => _animations.IsJumping = !IsGrounded();

        public void Move(Vector2 direction)
        {
            if (!_isDefeated)
            {
                bool isFlipX = _movement.Move(direction);
                SetDirectionX(isFlipX);
                float speedX = ((transform.position - _position) / Time.deltaTime).x;
                _animations.Speed = Mathf.Abs(speedX);
                _position = transform.position;
            }
        }

        public void Jump() => _movement.Jump(IsGrounded());

        private bool IsGrounded() => _groundCheck.IsGrounded;

        public void Attack(bool isPressDown)
        {
            if (!_isDefeated)
            {
                if (_isReloading) isPressDown = false;
                if (isPressDown)
                {
                    _attack.Attack(_directionX);
                }
                _animations.IsFire = isPressDown;
            }
        }

        private void SetDirectionX(bool isFlipX) => _directionX = isFlipX ? -1 : 1;

        public void TakeDamage()
        {
            _animations.TakeDamage();
        }

        public void Defeate()
        {
            if (!_isDefeated)
            {
                _animations.Defeate();
                _sounds.PlayDefeate();
                _isDefeated = true;
            }
        }

        private void BulletsCountChanged(int currentAmmo, bool isSetMaxBullet) 
        {
            if (currentAmmo == 0)
            {
                _isReloading = true;
                _sounds.PlayFire();
                _sounds.PlayReloading();
            }
            else
            {
                _isReloading = false;
                if (!isSetMaxBullet) _sounds.PlayFire();
            }
            _ammo.SetAmmo(currentAmmo);
        } 
    }
}
