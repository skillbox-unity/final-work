﻿using Assets.Scripts.GUI;
using UnityEngine;

namespace Assets.Scripts.Hero
{
    internal class Ammo : MonoBehaviour
    {
        [SerializeField] private AmmoText _ammoText;

        private int _ammoCount;

        public void SetAmmo(int ammo)
        {
            _ammoCount = ammo;
            _ammoText.SetAmmo(_ammoCount);
        }

        public int GetScore() => _ammoCount;
    }
}
