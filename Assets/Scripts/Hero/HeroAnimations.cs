using UnityEngine;

[RequireComponent(typeof(Animator))]

public class HeroAnimations : MonoBehaviour
{
    private Animator _animator;
    
    public float Speed { private get; set; }
    public bool IsJumping { private get; set; }
    public bool IsFire { private get; set; }

    private bool _isDefeated;

    void Start() => _animator = GetComponent<Animator>();    

    void Update()
    {
        _animator.SetFloat("speed", Speed);
        _animator.SetBool("isJumping", IsJumping);
        _animator.SetBool("isFire", IsFire);
    }

    public void TakeDamage()
    {
        _animator.SetTrigger("makedHurt");
    }

    public void Defeate()
    {
        _isDefeated = true;
        _animator.SetBool("isDefeated", _isDefeated);
    }
}
