﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Hero
{
    internal class GroundCheck : MonoBehaviour
    {
        [SerializeField] private LayerMask _groundMask;
        private float rayLenght = 0.15f;

        public bool IsGrounded { get; private set; }

        void Update() => CheckGround();

        private void CheckGround()
        {
            Vector3 rayStartPosition = transform.position;
            RaycastHit2D hit = Physics2D.Raycast(rayStartPosition, rayStartPosition + Vector3.down, rayLenght, _groundMask);

            if (hit) IsGrounded = true;
            else IsGrounded = false;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, transform.position + Vector3.down * rayLenght);
        }

    }
}
