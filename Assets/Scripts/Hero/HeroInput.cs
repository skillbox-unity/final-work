﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.Hero
{
    [RequireComponent(typeof(HeroActions))]
    internal class HeroInput : MonoBehaviour
    {
        [SerializeField] private Pause _pause;
        [SerializeField] private LayerMask _layer;
        private HeroActions _actions;
        

        void Start() => _actions = GetComponent<HeroActions>();

        void Update()
        {
            CheckMoveInput();
            CheckJumpInput();
            CheckFireInput();
        }

        private void CheckMoveInput()
        {
            float horizontal = Input.GetAxis("Horizontal");
            _actions.Move(new Vector2(horizontal, 0));
        }

        private void CheckJumpInput()
        {
            if (Input.GetKeyDown(KeyCode.Space)) _actions.Jump();
        }

        private void CheckFireInput() 
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (!_pause.Paused) _actions.Attack(Input.GetKey(KeyCode.Mouse0));
            }
            else
            {
                _actions.Attack(false);
            }
            
        }

    }
}
