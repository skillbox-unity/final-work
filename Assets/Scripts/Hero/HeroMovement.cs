using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class HeroMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;

    private SpriteRenderer _spriteRenderer;
    private Rigidbody2D _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public bool Move(Vector2 direction)
    {
        Vector3 offset = direction.normalized * _speed * Time.deltaTime;
        transform.position += offset;

        if (offset.x != 0)
        {
            _spriteRenderer.flipX = offset.x > 0 ? false : true;
        }
        return _spriteRenderer.flipX;
    }

    public void Jump(bool isGrounded)
    { 
        if (isGrounded) _rigidbody.AddForce(transform.up * _jumpForce);
    }
   
}
