using UnityEngine;
using UnityEngine.UI;

public class HpBar : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private Gradient gradient;
    [SerializeField] private Image fill;

    public void SetMaxHP(float maxHP)
    {
        slider.maxValue = maxHP;
        slider.value = maxHP;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    public void SetCurrentHP(float currentHP)
    {
        slider.value = currentHP;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
