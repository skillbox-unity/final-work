using Assets.Scripts.Objects.Bullet;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Characters
{
    public class CharacterAttack : MonoBehaviour
    {
        [SerializeField] private Bullet _bulletCreate;
        [SerializeField] private float _maxTimeToFire;
        [SerializeField] private Transform _bulletPointTransform;
        [SerializeField] private int _maxBulletsCount;
        [SerializeField] private float _timeForReload;
        
        private UnityEvent<int, bool> _onBulletsCountChange;
        private int _currentBulletsCount;
        private float _currentTimeToFire;
        private bool _canFire;
        private bool _isReloading;

        public event UnityAction<int, bool> OnBulletsCountChange
        {
            add => _onBulletsCountChange.AddListener(value);
            remove => _onBulletsCountChange.RemoveListener(value);
        }

        void Awake()
        {
            _onBulletsCountChange = new UnityEvent<int, bool>();
            _currentTimeToFire = 0;
            _canFire = false;
            _isReloading = false;
        }

        void Start()
        {
            SetMaxBullet();
        }

        void Update()
        {
            if (_currentTimeToFire > 0 && _canFire == false)
            {
                _currentTimeToFire -= Time.deltaTime;
                _canFire = false;
            }
            else
            {
                _canFire = true;
                _currentTimeToFire = _maxTimeToFire;
                if (_isReloading)
                {
                    _isReloading = false;
                    SetMaxBullet();
                }
            }
        }

        public void Attack(float directionX)
        {
            if (_canFire)
            {
                _bulletCreate.Create(_bulletPointTransform, directionX);
                _canFire = false;
                _currentBulletsCount--;
                _onBulletsCountChange.Invoke(_currentBulletsCount, false);
                if (_currentBulletsCount == 0)
                {
                    _currentTimeToFire = _timeForReload;
                    _isReloading = true;
                }
            }
        }

        private void SetMaxBullet()
        {
            _currentBulletsCount = _maxBulletsCount;
            _onBulletsCountChange.Invoke(_currentBulletsCount, true);
        }
    }
}