﻿using Assets.Scripts.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Hero
{
    internal class Scores : MonoBehaviour
    {
        [SerializeField] private ScoreText _scoreText;

        private int _totalScore;

        void Start()
        {
            _totalScore = 0;
            SetScoreTextAndPrefs();
        }

        public void AddPoints(int points)
        {
            _totalScore += points;
            SetScoreTextAndPrefs();
        }

        public int GetScore() => _totalScore;

        private void SetScoreTextAndPrefs()
        {
            _scoreText.SetScore(_totalScore);
            PlayerPrefs.SetInt("Score", _totalScore);
        }
    }
}
